package com.kshrd.jwtdemo.jwt.service;

import com.kshrd.jwtdemo.jwt.model.Users;
import com.kshrd.jwtdemo.jwt.payload.BaseApiResponse;
import com.kshrd.jwtdemo.jwt.payload.user.UserApiResponse;
import com.kshrd.jwtdemo.jwt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class UserServiceImplement implements UserService {

    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public BaseApiResponse<List<UserApiResponse>> findAllUsers() {

        BaseApiResponse<List<UserApiResponse>> response = new BaseApiResponse<>();

        try {

            List<UserApiResponse> value = userRepository.findAllUsers();

            if(!value.isEmpty()) {
                response.setMessage("Users has been found successfully.");
                response.setData(value);
                response.setStatus(true);
                response.setHttpStatus(HttpStatus.OK);
            } else {
                response.setMessage("Users is not found.");
                response.setData(value);
                response.setStatus(false);
                response.setHttpStatus(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            response.setMessage("Error: "+e.toString());
            response.setStatus(false);
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
        }

        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return response;

    }

    @Override
    public Users findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Users user = userRepository.findByUsername(username);
        if(user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return user;

    }

    @Override
    public BaseApiResponse<String> deleteUser(int userId) {

        BaseApiResponse<String> response = new BaseApiResponse<>();

        try {

            Boolean value = userRepository.deleteUser(userId);

            if(value) {
                response.setMessage("Users has been deleted successfully.");
                response.setStatus(true);
                response.setHttpStatus(HttpStatus.OK);
            } else {
                response.setMessage("Users is not delete.");
                response.setStatus(false);
                response.setHttpStatus(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            response.setMessage("Error: "+e.toString());
            response.setStatus(false);
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
        }

        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return response;

    }
}
