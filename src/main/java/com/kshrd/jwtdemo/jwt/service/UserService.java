package com.kshrd.jwtdemo.jwt.service;

import com.kshrd.jwtdemo.jwt.model.Users;
import com.kshrd.jwtdemo.jwt.payload.BaseApiResponse;
import com.kshrd.jwtdemo.jwt.payload.user.UserApiResponse;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService extends UserDetailsService {

    BaseApiResponse<List<UserApiResponse>> findAllUsers();

    Users findByUsername(String username);

    BaseApiResponse<String> deleteUser(int userId);

}
