package com.kshrd.jwtdemo.jwt.controller;

import com.kshrd.jwtdemo.jwt.config.jwt.JwtUtility;
import com.kshrd.jwtdemo.jwt.model.Users;
import com.kshrd.jwtdemo.jwt.payload.BaseApiResponse;
import com.kshrd.jwtdemo.jwt.payload.jwt.JwtApiResponse;
import com.kshrd.jwtdemo.jwt.payload.user.UserApiResponse;
import com.kshrd.jwtdemo.jwt.payload.user.UserLoginApiRequest;
import com.kshrd.jwtdemo.jwt.service.UserService;
import com.kshrd.jwtdemo.jwt.util.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class UserController {

    private Validation validation;

    private BCryptPasswordEncoder encoder;

    private AuthenticationManager authenticationManager;

    private JwtUtility jwtUtility;

    private UserService userService;

    @Autowired
    public void setValidation(Validation validation) {
        this.validation = validation;
    }

    @Autowired
    public void setEncoder(BCryptPasswordEncoder encoder) {
        this.encoder = encoder;
    }

    @Autowired
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Autowired
    public void setJwtUtility(JwtUtility jwtUtility) {
        this.jwtUtility = jwtUtility;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    public ResponseEntity<BaseApiResponse<List<UserApiResponse>>> findAllUser() {
        return ResponseEntity.ok(userService.findAllUsers());
    }

    @PostMapping("/users/authenticate")
    public ResponseEntity<BaseApiResponse<HashMap<String,Object>>> createAuthenticationToken(@RequestBody UserLoginApiRequest user) throws Exception {

        BaseApiResponse<HashMap<String,Object>> response = new BaseApiResponse<>();

        try {

            boolean username = validation.checkInputString(user.getUsername());
            boolean password = validation.checkInputPassword(user.getPassword());

            if (username || password) {

                if (username) {
                    if (password)
                        response.setMessage("Username and password are invalid.");
                    else
                        response.setMessage("Username is invalid.");
                } else {
                    response.setMessage("Password should have at least 8 characters.");
                }

                response.setStatus(false);
                response.setHttpStatus(HttpStatus.BAD_REQUEST);
                return ResponseEntity.ok(response);

            } else {

                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(user.getUsername(),user.getPassword());
                authenticationManager.authenticate(usernamePasswordAuthenticationToken);

                final UserDetails userDetails = userService.loadUserByUsername(user.getUsername());
                Users users = userService.findByUsername(user.getUsername());
                final String token = jwtUtility.generateToken(userDetails);

                JwtApiResponse value = new JwtApiResponse(token);

                HashMap<String,Object> data = new HashMap<>();
                data.put("id",users.getId());
                data.put("username",userDetails.getUsername());
                data.put("roles",userDetails.getAuthorities());
                data.put("data",value);

                response.setMessage("Login is successfully.");
                response.setData(data);
                response.setStatus(true);
                response.setHttpStatus(HttpStatus.OK);

            }

        } catch (BadCredentialsException e) {
            response.setMessage("Invalid username or password.");
            response.setStatus(false);
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
        }

        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);

    }

    @DeleteMapping("/users/{userId}")
    public ResponseEntity<BaseApiResponse<String>> deleteUser(@PathVariable("userId") int userId) {
        return ResponseEntity.ok(userService.deleteUser(userId));
    }

}
