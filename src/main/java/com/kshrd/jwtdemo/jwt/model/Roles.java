package com.kshrd.jwtdemo.jwt.model;

import org.springframework.security.core.GrantedAuthority;

public class Roles implements GrantedAuthority {

    private Integer id;

    private String name;

    public Roles() {}

    public Roles(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getAuthority() {
        return "ROLE_"+name;
    }
}
