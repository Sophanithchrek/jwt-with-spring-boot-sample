package com.kshrd.jwtdemo.jwt.repository;

import org.apache.ibatis.jdbc.SQL;

public class UserProvider {

    public String findByUsername() {
        return new SQL(){{
            SELECT ("u.id, u.username, u.email, u.password, u.status, u.role_id, r.name");
            FROM ("Users u INNER JOIN Roles r ON u.role_id = r.id");
            WHERE ("u.username = #{username}");
        }}.toString();
    }

    public String findAllUsers() {
        return new SQL(){{
            SELECT("u.id, username, email, status, role_id, r.name");
            FROM("Users u INNER JOIN Roles r ON u.role_id = r.id");
            WHERE("u.status = '1'");
        }}.toString();
    }

    public String deleteUser() {
        return new SQL(){{
            UPDATE("Users");
            SET("status = '2'");
            WHERE("id = #{userId}");
        }}.toString();
    }

}
