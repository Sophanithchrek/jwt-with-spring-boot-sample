package com.kshrd.jwtdemo.jwt.repository;

import com.kshrd.jwtdemo.jwt.model.Roles;
import com.kshrd.jwtdemo.jwt.model.Users;
import com.kshrd.jwtdemo.jwt.payload.user.UserApiResponse;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {

    @SelectProvider(type = UserProvider.class, method = "findByUsername")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "roles", column = "role_id", many = @Many(select = "findRoleByUserId"))
    })
    Users findByUsername(@Param("username") String username);

    @Select("SELECT id, name FROM Roles WHERE id = #{id}")
    List<Roles> findRoleByUserId(int id);

    @SelectProvider(type = UserProvider.class, method = "findAllUsers")
    @Results({
            @Result(property = "roles.id", column = "role_id"),
            @Result(property = "roles.name", column = "name")
    })
    List<UserApiResponse> findAllUsers();

    @DeleteProvider(type = UserProvider.class, method = "deleteUser")
    Boolean deleteUser(@Param("userId") int userId);

}
