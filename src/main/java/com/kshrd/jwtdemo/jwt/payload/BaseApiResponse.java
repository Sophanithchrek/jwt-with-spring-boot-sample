package com.kshrd.jwtdemo.jwt.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;

public class BaseApiResponse<T> {

    private String message;

    @JsonInclude(JsonInclude.Include.NON_NULL)

    private T data;

    private Boolean status;

    private HttpStatus httpStatus;

    private Timestamp timestamp;

    public BaseApiResponse() {}

    public BaseApiResponse(String message, T data, Boolean status, HttpStatus httpStatus, Timestamp timestamp) {
        this.message = message;
        this.data = data;
        this.status = status;
        this.httpStatus = httpStatus;
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}